module.exports = (req, res, next) => {
    if (req.user.role === 1) return next();
    else res.send("403 - anda tidak memiliki hak akses");
}