const { User, UserBiodata } = require("../models");

module.exports = {
    index: (req, res) => {
        UserBiodata.belongsTo(User, { foreignKey: 'user_game_id' });
        User.hasOne(UserBiodata, { foreignKey : 'user_game_id' });
    
        User.findAll({
            include: [{
                model: UserBiodata
               }]
        })
            .then(users => {
                res.render("users/index", {
                    users
                });
            });
    },
    create: (req, res) => {
        res.render("users/create");
    },
    store: (req, res) => {
        User.create({
            username: req.body.username,
            password: req.body.password
        })
            .then(user => {
                UserBiodata.create({
                    user_game_id: user.id,
                    name: req.body.name,
                    phone: req.body.phone,
                    address: req.body.address
                })
                    .then(()  => {
                        res.send("user berhasil dibuat")
                    })
                    .catch(err => {
                        console.log(err);
                        res.send("gagal buat user biodata");
                    })
            })
            .catch(err => {
                console.log(err);
                res.send("gagal buat user");
            });
    },
    show: (req, res) => {
        UserBiodata.belongsTo(User, { foreignKey: 'user_game_id' });
        User.hasOne(UserBiodata, { foreignKey : 'user_game_id' });
    
        User.findOne({
            where: { id: req.params.id },
            include: [{
                model: UserBiodata
               }]
        })
            .then(user => {
                console.log(user);
                res.render("users/show", { 
                    user
                });
            });
    },
    edit: (req, res) => {
        UserBiodata.belongsTo(User, { foreignKey: 'user_game_id' });
        User.hasOne(UserBiodata, { foreignKey : 'user_game_id' });
    
        User.findOne({
            where: { id: req.params.id },
            include: [{
                model: UserBiodata
               }]
        })
            .then(user => {
                console.log(user);
                res.render("users/edit", { 
                    user
                });
            });
    },
    update: (req, res) => {
        User.update({
            username: req.body.username
        }, {
            where: { id: req.params.id }
        })
            .then(user => {
                UserBiodata.update({
                    name: req.body.name,
                    phone: req.body.phone,
                    address: req.body.address
                }, {
                    where: { user_game_id: req.params.id }
                })
                    .then(()  => {
                        res.send("user berhasil diupdate")
                    })
                    .catch(err => {
                        console.log(err);
                        res.send("gagal update user biodata");
                    })
            })
            .catch(err => {
                console.log(err);
                res.send("gagal update user");
            });
    },
    destroy: (req, res) => {
        User.destroy({
            where: { id: req.params.id }
        })
            .then(user => {
                UserBiodata.destroy({
                    where: { user_game_id: req.params.id }
                })
                    .then(()  => {
                        res.send("user berhasil dihapus")
                    })
                    .catch(err => {
                        console.log(err);
                        res.send("gagal hapus user biodata");
                    })
            })
            .catch(err => {
                console.log(err);
                res.send("gagal hapus user");
            });
    }
}