const { User, UserBiodata } = require("../models");
const passport = require("../lib/passport");

module.exports = {
    register: (req, res) => {
        User.register(req.body)
            .then(user => {
                UserBiodata.create({
                    user_game_id: user.id,
                    name: req.body.name,
                    phone: req.body.phone,
                    address: req.body.address
                })
                    .then(() => res.redirect("/login"));
            })
            .catch(err => next(err));
    },
    login: passport.authenticate("local", {
        successRedirect: "/game",
        failureRedirect: "/login",
        failureFlash: false
    })
}